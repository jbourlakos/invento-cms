namespace :data do
    namespace :modules do
        desc "List all extracted modules."
        task :list do
            # sh "sudo mkdir -p ./data/web/var/www/html/modules/extracted"
            # sh "sudo ls ./data/web/var/www/html/modules/extracted"
            abort "Unsupported. To be redefined, based on volumes."
        end

    end
end