namespace :data do

    namespace :clear do

        desc "Clear all the data."
        task :all => ["compose:down"] + @docker_services do
          task_run "project:files:fix_owner"
        end

    end
    
end