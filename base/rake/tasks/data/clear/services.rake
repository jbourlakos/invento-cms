namespace :data do

    namespace :clear do

        @docker_services.each do |service|
            desc "Clear data for #{service} service."
            task service do
                (@project['docker-compose']['services'][service]['volumes'] || []).
                    map { |entry| entry.split(':')[0] }.
                    each { |volume_name| sh "#{D} volume rm #{PREFIX}_#{volume_name}" }
            end

        end

    end

end