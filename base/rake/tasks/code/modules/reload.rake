namespace :code do

    namespace :modules do

        desc "Reload custom themes in web container"
        task :reload, [:module_name, :module_type, :folder] => [:unload, :load] do |task, args|
          args.with_defaults(module_type: 'modules', folder: 'custom')
          #do nothing further
        end


    end

end


