namespace :code do

  namespace :modules do

    desc "Unloads module from the web container"

    task :unload, [:module_name, :module_type, :folder] do |task, args|
      args.with_defaults(module_type: 'modules', folder: 'custom')

      # uninstall module from Drupal
      begin
        task_run(
          "container:web:do", 
          "drush pm-uninstall -y #{args[:module_name]}"
        )
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:unload: #{e}"
      end

      # purge it from its directory
      begin
        task_run(
          "container:web:do", 
          "rm -rf #{args[:module_type]}/#{args[:folder]}/#{args[:module_name]}"
        )
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:unload: #{e}"
      end

      # remove it from development directory
      begin
        task_run(
          "container:web:do", 
          "rm -rf ../development/#{args[:module_name]}"
        )
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:unload: #{e}"
      end

    end


  end

end


