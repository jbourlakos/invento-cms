namespace :code do

  namespace :modules do

    task :updatedb do
      # update database for drupal
      task_run("container:web:do", "drush updb --entity-updates -y")
    end


  end

end