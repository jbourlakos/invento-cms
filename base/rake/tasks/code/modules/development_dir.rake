namespace :code do

  namespace :modules do

    task :development_dir do
      result = task_out 'volume:web:web_development:mountpoint'
      sh "sudo file #{result}"
    end

  end
end