namespace :code do

    namespace :modules do

        desc "Apply code/modules in volume and clear cache"
        task :load, [:module_name, :module_type, :folder] => [:development_dir]  do |task, args|
            args.with_defaults(module_type: 'modules', folder: 'custom')

            # get volume host path
            volume_path = task_out('volume:web:web_development:mountpoint')

            # copy the new module
            begin
                sh "sudo cp -r code/#{args[:module_type]}/#{args[:module_name]} -t #{volume_path}"
            rescue Exception => e
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end


            begin
                task_run "container:web:do","mkdir -p ./#{args[:module_type]}/#{args[:folder]}"
            rescue
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end

            begin
                task_run "container:web:do","cp -r ../development/#{args[:module_name]} ./#{args[:module_type]}/#{args[:folder]}/"
            rescue
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end

            # fix user
            begin
                task_run "container:web:do", "chown -R www-data:www-data #{args[:module_type]}"
            rescue Exception => e
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end

            # fix privileges
            begin
                task_run "container:web:do", "chmod -R +r #{args[:module_type]}"
            rescue Exception => e
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end

            # drush enable modules
            begin
                task_run "container:web:do", "drush pm-enable -y #{args[:module_name]}"
            rescue Exception => e
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end

            # entity updates
            if args[:module_type] == 'modules'
                begin
                    task_run "container:web:do", "drush updb --entity-updates -y"
                rescue Exception => e
                    $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
                end
            end

            # rebuild cache in container
            begin
                task_run "container:web:do", "drush cache-rebuild"
            rescue Exception => e
                $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
            end

        end
    end
end