namespace :code do

    namespace :modules do

        desc "Lists loaded modules by subfolder(default: custom)"
        task :list_loaded, [:module_type, :folder] do |task, args|
          args.with_defaults(module_type: 'modules', folder: 'custom')
          task_run("container:web:do", "ls #{args[:module_type]}/#{args[:folder]}")
        end

    end

end


