namespace :code do

  namespace :modules do

    desc "Quikly reload module in volume (without uninstalling, enabling or cache rebuilding)"
    task :qreload, [:module_name, :module_type, :folder] => [:development_dir]  do |task, args|
      args.with_defaults(module_type: 'modules', folder: 'custom')

      # purge module from its directory
      begin
        task_run(
          "container:web:do", 
          "rm -rf #{args[:module_type]}/#{args[:folder]}/#{args[:module_name]}"
          )
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:unload: #{e}"
      end

      # remove it from development directory
      begin
        task_run(
          "container:web:do", 
          "rm -rf ../development/#{args[:module_name]}"
          )
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:unload: #{e}"
      end


      # get volume host path
      volume_path = task_out('volume:web:web_development:mountpoint')

      # copy the new module
      begin
        sh "sudo cp -r code/#{args[:module_type]}/#{args[:module_name]} -t #{volume_path}"
      rescue Exception => e
        $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
      end

      # ensure proper directory
      begin
        task_run "container:web:do","mkdir -p ./#{args[:module_type]}/#{args[:folder]}"
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
      end

      # copy module to its directory
      begin
        task_run "container:web:do","cp -r ../development/#{args[:module_name]} ./#{args[:module_type]}/#{args[:folder]}/"
      rescue
        $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
      end

      # fix user
      begin
        task_run "container:web:do", "chown -R www-data:www-data #{args[:module_type]}"
      rescue Exception => e
        $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
      end

      # fix privileges
      begin
        task_run "container:web:do", "chmod -R +r #{args[:module_type]}/#{args[:module_name]}"
      rescue Exception => e
        $stderr.puts "Warning: code:#{args[:module_type]}:load: #{e}"
      end

    end
  end
end