namespace :code do

    namespace :themes do

        task :default_current_theme, [:default_theme_name] do |task, args|
            args.with_defaults(default_theme_name:'bartik')
            begin
                # find current default theme
                default_theme_name = task_out 'drupal:themes:default'
                # if it's the same
                if default_theme_name != args[:default_theme_name]
                    # change to another theme # TODO: fix bartik, shouldn't alway be set as default
                    task_run "container:web:do", "drush config-set system.theme default -y #{args[:default_theme_name]}"
                end
            rescue
                # nothing
            end
        end


    end

end


