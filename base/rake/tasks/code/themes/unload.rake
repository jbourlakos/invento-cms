namespace :code do

    namespace :themes do

        desc "Unloads custom themes in web container"
        task :unload, [:module_name, :folder] => [:default_current_theme] do |task, args|
            args.with_defaults(module_type: 'themes', folder: 'custom')
            task_run 'code:modules:unload', "#{args[:module_name]}", "#{args[:module_type]}", "#{args[:folder]}"
        end


    end

end


