namespace :code do

    namespace :themes do

        desc "Apply code/themes in volume and clear cache"
        task :load, [:module_name, :folder] do |task, args|
            args.with_defaults(module_type:'themes', folder: 'custom')
            # Load as module
            task_run 'code:modules:load', "#{args[:module_name]}", "#{args[:module_type]}", "#{args[:folder]}"
            # Set as default theme
            task_run 'code:themes:default_current_theme', "#{args[:module_name]}"
        end
    end
end