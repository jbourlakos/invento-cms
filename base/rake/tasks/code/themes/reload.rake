namespace :code do

    namespace :themes do

        desc "Reload custom themes in web container"
        task :reload, [:module_name, :module_type, :folder] => [:unload, :load] do |task, args|
          args.with_defaults(module_type: 'themes')
            #nothing
        end


    end

end


