namespace :code do

    namespace :themes do

        desc "Lists loaded custom themes"
        task :list_loaded => [:"code:modules:list_loaded"] do
          args.with_defaults(module_type:'themes', folder:'custom')
        end

    end
end
