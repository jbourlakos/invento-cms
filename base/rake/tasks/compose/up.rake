namespace :compose do

    desc "Start a new docker composition with new containers or for a specific SERVICE."

    task :up, [:params] do |task, args|
        args.with_defaults(params:'')
        _sh_orig "#{DC} up #{args[:params]}"
    end
    

end