namespace :compose do

    desc "Print configuration for the composition in YAML."

    task :yaml do
        puts @project["docker-compose"].to_yaml
    end


end