namespace :compose do

    desc "Start composition with existing containers."

    task :start, [:params] do |task, args|
      args.with_params(params: '')
      _sh_orig "#{DC} start #{args[:params]}"
    end


end