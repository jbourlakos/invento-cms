namespace :compose do

    desc "Shutdown and remove containers."

    task :down, [:params] do |task, args|
        args.with_defaults(params:'')
        _sh_orig "#{DC} down #{args[:params]}"
    end

end