namespace :compose do
    
    desc "Print configuration for the composition in Ruby hash."

    task :ruby do |task, args|
        pp @project["docker-compose"]
    end

end