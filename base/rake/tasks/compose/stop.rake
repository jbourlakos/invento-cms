namespace :compose do

    desc "Stop composition without removing containers."

    task :stop, [:params] do |task, args|
        args.with_params(params:'')
        _sh_orig "#{DC} stop #{args[:params]}"
    end




end