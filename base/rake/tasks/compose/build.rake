namespace :compose do

    desc "Build new images for composition. Params as in: docker-compose build --help"

    task :build, [:params] do |task, args|
      args.with_defaults(params:'')
      _sh_orig "#{DC} build #{args[:params]}"
    end

end