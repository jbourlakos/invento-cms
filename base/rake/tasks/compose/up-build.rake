namespace :compose do

    desc "Build images and start a new composition."

    task :"up-build" => [:build, :up]

end
