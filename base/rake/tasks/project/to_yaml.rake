namespace :project do
  desc 'Print project configuration in YAML format'
  task :to_yaml do
    puts @project.to_yaml
  end
end