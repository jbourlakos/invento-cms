namespace :project do
  namespace :git do
    namespace :branch do
      namespace :current do

        task :ref do
          sh "#{GIT} rev-parse --short=8 HEAD"
        end

      end
    end
  end
end