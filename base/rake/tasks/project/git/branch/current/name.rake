namespace :project do
  namespace :git do
    namespace :branch do
      namespace :current do

        task :name do
          sh "#{GIT} rev-parse --abbrev-ref=strict HEAD"
        end

      end
    end
  end
end