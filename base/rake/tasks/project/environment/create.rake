namespace :project do 
    namespace :environment do 

        desc 'Create a new project environment'

        task :create, [:new_environment, :environment_prefix, :parent_environment] => [:proper_project_environments] do |t,a|
            a.with_defaults(parent_environment: @project['current_environment'])
            abort('A name for the new environment is required.') unless a.new_environment
            abort("Environment '#{a.new_environment}' already exists in project.") if @project['environments'].include? a.new_environment
            abort("Parent environment '#{a.parent_environment}' doesn't exist.") unless @project['environments'].include? a.parent_environment
            # create copy for new environment
            ## TODO
            update_project do |proj|
              proj['environments'] <<= a.new_environment
              proj[a.new_environment] = {}
              proj[a.new_environment]['prefix'] = a.environment_prefix
            end
        end

    end
end