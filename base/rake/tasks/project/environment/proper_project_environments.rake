namespace :project do 
    namespace :environment do 

        task :proper_project_environments => [ 'Rakefile', BASE_PROJECT_YAML_FILE ] do 
          unless @project
            abort "Could not create project object. './Rakefile' may require repair."
          end
          unless @project['environments'] and @project['environments'].size > 0
            abort "At least one environment is required. '#{BASE_PROJECT_YAML_FILE}' may require repair."
          end
          unless @project['environments'].include? @project['current_environment']
            abort "Current environment '#{@project['current_environment']} doesn't belong in declared project environments. '#{BASE_PROJECT_YAML_FILE}' may require repair."
          end
        end

    end
end