namespace :project do

    namespace :files do
        desc "Convert ownership of EVERY file in the project folder to current user: #{ENV['USER']}."
        task :fix_owner do
            sh "sudo find . -not -user `id -un` | xargs -r -n 100 sudo chown `id -un`:`id -gn` | true"
        end
    end

end