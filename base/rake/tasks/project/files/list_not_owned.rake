namespace :project do

    namespace :files do
        desc "List any files that are not owned by #{ENV['USER']}."
        task :list_not_owned do
            sh "sudo find . -not -user `id -un`"
        end

    end


end



