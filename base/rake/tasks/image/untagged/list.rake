namespace :image do

    namespace :untagged do
        desc "List all untagged images."
        task :list do
            untagged_images = `#{D} images -q -f dangling=true`
            untagged_images.split('\n').each do |ui|
                puts "#{ui}"
            end
        end
    end
end