namespace :image do

    namespace :untagged do
        
        desc "Clear EVERY untagged image on the WHOLE system."
        task :clear do
            if warn("This is will delete EVERY untagged image on the system.")
                untagged_images = `#{D} images -q -f dangling=true`
                sh "#{D} rmi #{untagged_images.split("\n").join(' ')}"
            else 
                puts "Aborted."
            end
        end
    end

end
