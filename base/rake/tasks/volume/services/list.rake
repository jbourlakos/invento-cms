namespace :volume do

    @docker_services.each do |service|

        namespace service do

            desc  "Lists volumes of service #{service}"

            task :list do
                self.send("#{service}_volumes").each do |v|
                    puts v
                end
            end


        end

    end

end