namespace :volume do

  @docker_services.each do |service|

    namespace service do

      send("#{service}_volumes").each do |volume|

        namespace volume do

          desc "Mountpoint (path) of #{service} #{volume} volume"

          task :mountpoint do
            out = task_out "volume:#{service}:#{volume}:to_yaml"
            volume_yaml = YAML.load(out)
            puts volume_yaml[0]['Mountpoint']
          end

        end

      end

    end

  end

end
