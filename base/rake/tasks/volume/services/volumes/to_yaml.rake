namespace :volume do

  @docker_services.each do |service|

    namespace service do

      send("#{service}_volumes").each do |volume|

        namespace volume do

          task :to_yaml do
            sh "#{D} volume inspect #{PREFIX}_#{volume}"
          end

        end

      end

    end

  end

end
