namespace :volume do 

  desc 'Lists project associated volumes'

  task :list do
    volume_names.sort.each do |r|
      puts r
    end
  end

end