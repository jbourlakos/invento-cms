namespace :drupal do
  namespace :themes do

    desc 'The current default Drupal theme'

    task :default do
      task_run('container:web:do','drush config-get system.theme default')
    end

  end
end