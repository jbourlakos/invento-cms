namespace :drupal do
    namespace :modules do


        desc 'Update database (Web container).'
      
        task :updb do |task, args|
          task_run("container:web:do", "drush updb")
        end


    end
end
