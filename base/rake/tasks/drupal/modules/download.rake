namespace :drupal do
    namespace :modules do


        desc 'Download modules in Drupal (Web container).'
      
        task :download, [:module_name] do |task, args|
          module_name = args[:module_name]
          task_run("container:web:do", "drush pm-download -y #{module_name}")
        end


    end
end
