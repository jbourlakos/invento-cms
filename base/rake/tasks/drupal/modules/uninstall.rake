namespace :drupal do
    namespace :modules do


        desc 'Uninstall module in Drupal (Web container).'
      
        task :uninstall, [:module_name] do |task, args|
          module_name = args[:module_name]
          task_run("container:web:do", "drush pm-uninstall -y #{module_name}")
        end


    end
end
