namespace :drupal do
    namespace :modules do


        desc 'Update database including entities (Web container).'
      
        task :updb_entities do |task, args|
          task_run("container:web:do", "drush updb --entity-updates -y")
        end


    end
end
