namespace :drupal do
    namespace :modules do


        desc 'Reset a module in Drupal (Web container).'
      
        task :reset, [:module_name] => [:uninstall, :enable] do |task, args|
        end


    end
end
