namespace :drupal do
  namespace :modules do


    desc 'Status of module in Drupal (Web container).'
    task :status, [:module_name] do |task, args|
      puts task_out("container:web:do", "drush pm-info #{args[:module_name]}")
    end


  end
end
