namespace :drupal do
    namespace :modules do


        desc 'Enable module in Drupal (Web container).'
      
        task :enable, [:module_name] do |task, args|
          module_name = args[:module_name]
          task_run("container:web:do", "drush pm-enable -y #{module_name}")
        end


    end
end
