namespace :drupal do

    task :clear_cache do
        task_run 'container:web:do', 'drush cache-rebuild'
    end

end