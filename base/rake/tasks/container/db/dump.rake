namespace :container do


    namespace :db do

        desc "Print MySQL Dump"

        task :dump do
            task_run "container:db:do", 'mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"'
        end

    end

end