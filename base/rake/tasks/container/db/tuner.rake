namespace :container do


    namespace :db do

        desc "Print MySQL Tuner results"

        task :tuner do
            task_run "container:db:do", "perl /root/MySQLTuner-perl/mysqltuner.pl"
        end

    end

end