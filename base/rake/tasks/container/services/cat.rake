
namespace :container do

    @docker_services.each do |service|

        namespace service do

            desc "Extract (copy) a file from #{service} to the specified location."

            task :cat, [:container_path, :local_path] do |task, args|
                container_path = args[:container_path]
                local_path = args[:local_path]
                if local_path and File.exist? local_path and ! warn "This task will overwrite existing file '#{local_path}'."
                    abort 'Aborted. Nothing changed.'
                end
                task_run "container:#{service}:do", "cat #{container_path}", nil, local_path
            end

        end

    end

end