
namespace :container do

    @docker_services.each do |service|

        namespace service do

            desc "Print #{service} logs on stdout."

            task :logs do
                sh "#{D} logs #{self.send("#{service}_service_id")}"
            end

        end

    end

end