
namespace :container do

    @docker_services.each do |service|

        namespace service do

            desc "Execute a command in #{service} shell."

            task :do, [:command] do |task, args|
                sh "#{DEXECI} #{self.send("#{service}_service_id")} #{args[:command]}"
            end


        end

    end

end