
namespace :container do

    @docker_services.each do |service|

        namespace service do

            desc "Terminal access to #{service} shell."

            task :terminal do
                _sh_orig "#{DEXECIT} #{self.send("#{service}_service_id")} bash"
            end

        end

    end

end