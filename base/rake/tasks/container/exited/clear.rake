namespace :container do

    namespace :exited do
        desc "Delete EVERY exited container."
        task :clear do
            if warn("This is will delete EVERY exited container on the WHOLE system.")
                exited_containers = `#{D} ps -q -f status=exited`
                exited_containers.split.each do |ec|
                    sh "#{D} rm #{ec}"
                end
            else 
                puts "Aborted."
            end
        end
    end


end
