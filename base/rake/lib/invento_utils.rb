@docker_services.each do |service|

  # define #*_service_id
  define_method("#{service}_service_id") do
      (%x[#{DC} ps -q #{service}]).strip
  end

  # define #*_volume method
  define_method("#{service}_volumes") do
    ( @project['docker-compose']['services'][service]['volumes'] || [])
      .map { |entry| entry.split(':')[0] }
  end

end



def volume_names
  result = []
  # get global declared first
  result += @project['docker-compose']['volumes'].keys
  # get per service specific; duplicates may occur    
  result += @docker_services
    .map { |service| self.send("#{service}_volumes") }
    .inject([], :+)
  # convert to set
  result.to_set
end



def warn msg
    puts msg
    print "Are you sure? [y/N] "
    ((response = STDIN.gets.chomp).downcase != 'y') ? false : true
end



def update_project(&block)
  if block_given?
    # project_file = PROJECT_HOME + '/' + PROJECT_YAML_FILE
    project_file = PROJECT_YAML_FILE
    derived_project = YAML.load(File.read(project_file))
    yield(derived_project)
    File.open(project_file, 'w') {|f| f.write derived_project.to_yaml }
    @project.merge!(derived_project)
  end
end



def stdio_tap stdout=$stdout, stderr=$stderr, stdin=$stdin, &block
  # enable file-path arguments as files
  stdout = File.open stdout, 'w' if stdout and stdout.instance_of? String
  stderr = File.open stderr, 'w' if stderr and stderr.instance_of? String
  stdin = File.open stdin, 'r' if stdin and stdin.instance_of? String

  # validate redirect arguments
  raise "Invalid stdout object (#{stdout.class} - #{stdout})" unless stdout.respond_to? :write # TODO: better define IO writable
  raise "Invalid stderr object (#{stderr.class} - #{stderr})" unless stderr.respond_to? :write # TODO: better define IO writable
  raise "Invalid stdin object (#{stdin.class} - #{stdin})" unless stdin.respond_to? :read # TODO: better define IO readable

  # save current streams
  previous_stdout = $stdout
  previous_stderr = $stderr
  previous_stdin = $stdin

  # redirect i/o/e
  $stdout = stdout if stdout
  $stderr = stderr if stderr
  $stdin = stdin if stdin

  # execute
  yield

  # recover i/o/e
  $stdout = previous_stdout if stdout
  $stderr = previous_stderr if stderr
  $stdin = previous_stdin if stdin

end

def task_run task_str, *args
  task_tap(task_str, $stdout, $stderr, $stdin, *args)
end

def task_tap task_str, stdout=$stdout, stderr=$stderr, stdin=$stdin, *args
  stdio_tap(stdout, stderr, stdin) do
    Rake::Task[task_str].reenable
    Rake::Task[task_str].invoke(*args)
  end
end


def task_out task_str, args_str='', chop=true
  task_tap(task_str, out = StringIO.new('','w'), $stderr, $stdin, args_str)
  out.close
  chop ? out.string.chop : out.string
end