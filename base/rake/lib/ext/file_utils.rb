module FileUtils

  # override FileUtil's #sh method to support Docker environment

  alias_method :_sh_orig, :sh

  def sh(*args)
    bufsiz = 2048
    docker_environment = ENV.select{ |k,v| k =~ /^DOCKER_/ }

    Open3.popen3(docker_environment, *args) do |sin, sout, serr, thr|
      sin.close
      Thread.new do
        sout.each {|l| $stdout.puts l}
      end.join
      Thread.new do
        serr.each {|l| $stderr.puts l}
      end.join
    end
    # 
    # 
    # 
    # Open3.popen3(docker_environment, *args) do |sin, sout, serr, thr|
    #   sin.close_write
    #   begin
    #     files = {sout => $stdout, serr => $stderr}
    #     until files.keys.find { |f| !f.eof }.nil? do
    #       in_ready = IO.select(files.keys)
    #       out_ready = IO.select(files.values)
    #       if in_ready and out_ready
    #         readable = in_ready[0]
    #         readable.each do |f|
    #           fileno = f.fileno
    #           begin
    #             data = f.read_nonblock(bufsiz)
    #             files[f].write_nonblock(bufsiz)
    #           rescue EOFError => e 
    #             # ok
    #           end
    #         end
    #       end
    #     end
    #   rescue IOError => e
    #     puts e
    #   end
    #   exit_status = thr.value
    # end
    # 
    # 
    # 
    # Open3.popen3(docker_environment, *args) do |sin, sout, serr, thr|
    #   out_finished = err_finished = false
    #   until out_finished and err_finished
    #     begin
    #       out_line = sout.read_nonblock(2048)
    #     rescue IO::WaitReadable
    #       IO.select([sout])
    #       retry
    #     rescue EOFError => ex
    #       out_finished = true
    #     rescue 
    #       # do nothing
    #     end
    #     begin
    #       result = $stdout.write_nonblock(out_line)
    #     rescue IO::WaitWritable, Errno::EINTR
    #       IO.select(nil, [$stdout])
    #       retry
    #     end
    #   end
    #   exit_status = thr.value
    # end
    # 
    # 
    # 
    # stdout, stderr, status = Open3.capture3(
    #   ENV.select{ |k,v| k =~ /^DOCKER_/ },
    #   *args
    # )
    # if block_given?
    #   yield(stdout, stderr, status)
    # else
    #   $stdout.write(stdout)
    #   $stderr.write(stderr)
    # end
    # status
    # 
    # 
    # 
    # _sh_orig ENV.select{ |k,v| k =~ /^DOCKER_/ }, *args
  end

end
