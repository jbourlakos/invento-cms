#!/bin/bash


###
# available environment from dockerfiles, docker-compose and .env
# (exemplary values)
###

# APACHE_CONFDIR=/etc/apache2
# APACHE_ENVVARS=/etc/apache2/envvars
# CONFIGURATIONS_DIRECTORY=/root/conf
# DRUPAL_ACCOUNT_NAME=admin
# DRUPAL_ACCOUNT_PASS=12345
# DRUPAL_FILES_DIRECTORY_RELATIVE=sites/default/files
# DRUPAL_MD5=55a53cb43284b3d710a2742d458fc1da
# DRUPAL_PRIVATE_DIRECTORY_RELATIVE=/var/www/private
# DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE=sites/default
# DRUPAL_SITE_NAME=Invento CMS
# DRUPAL_VERSION=8.4.3
# E=development
# GPG_KEYS=A917B1ECDA84AEC2B568FED6F50ABC807BD5DCD0 528995BFEDFBA7191D46839EF9BA0ADA31CBD89E
# HOME=/root
# HOSTNAME=7661013ebce9
# MYSQL_DATABASE=cmsbase
# MYSQL_HOST=db
# MYSQL_PASSWORD=123456
# MYSQL_ROOT_PASSWORD=112358
# MYSQL_USER=drupal
# PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# PHPIZE_DEPS=autoconf        dpkg-dev        file        g++         gcc         libc-dev        make        pkg-config      re2c
# PHP_ASC_URL=https://secure.php.net/get/php-7.1.12.tar.xz.asc/from/this/mirror
# PHP_CFLAGS=-fstack-protector-strong -fpic -fpie -O2
# PHP_CPPFLAGS=-fstack-protector-strong -fpic -fpie -O2
# PHP_EXTRA_BUILD_DEPS=apache2-dev
# PHP_EXTRA_CONFIGURE_ARGS=--with-apxs2
# PHP_INI_DIR=/usr/local/etc/php
# PHP_LDFLAGS=-Wl,-O1 -Wl,--hash-style=both -pie
# PHP_MD5=
# PHP_SHA256=a0118850774571b1f2d4e30b4fe7a4b958ca66f07d07d65ebdc789c54ba6eeb3
# PHP_URL=https://secure.php.net/get/php-7.1.12.tar.xz/from/this/mirror
# PHP_VERSION=7.1.12
# SITES_DIRECTORY=/var/www
# TERM=xterm
# WORKING_DIRECTORY=/var/www/html


###
# script configuration
###

db_reconnection_time=3 # seconds
web_server_user='www-data'
web_server_group='www-data'
grant_query="GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';"
settings_overrides_file_basename='settings.overrides.php'
settings_lock_file_basename='.settings.lock'
files_htaccess_basename='files.htaccess'
html_htaccess_basename='html.htaccess'



###
# global variables and functions
###

stage=

last_command_status=


function report () {
    report_n "$1"
    echo # new line
}


function report_n {
    message=$1
    if [ ! -z "$message" ] ; then
        echo -n "[$stage]" "$message"
    else
        echo -n # print empty line
    fi
}


function set_stage () {
    stage=$1
    report "New stage: ${stage}"
}


function status {
    last_command_status=$?
}


function print_status {
    status
    if [ $last_command_status -ne 0 ] ; then 
        echo "[fail - ${last_command_status}]"
    else
        echo "[ok]"
    fi
    echo
}


function exit_on_error {
    ok_status=$2
    ok_status=${error_status:-0}
    message=$1
    if [ $last_command_status -ne $ok_status ] ; then
        report "$message"
        echo 'Exiting...'
        exit
    fi
}



###
# the script
###



set_stage 'PRE'



which drush
status
exit_on_error

which mysql
status
exit_on_error

which mysqladmin
status
exit_on_error



cd ${WORKING_DIRECTORY}



# wait on database server
report "Waiting on database server: ${MYSQL_HOST}..."
until mysqladmin --host=${MYSQL_HOST} --password=${MYSQL_ROOT_PASSWORD} ping ; do
    report "Connection to database failed: $(date +%T)"
    report_n "Retrying in "
    for i in $(seq $db_reconnection_time -1 1); do
        echo -n "$i "
        sleep 1
    done
    echo # new line
done


# create services.yml
if [ ! -f ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/services.yml ] ; then
    report "Creating initial services.yml..."
    if [ ! -f ${CONFIGURATIONS_DIRECTORY}/services.${E}.yml ] ; then
        report "Could not find proper services.yml for ${E} environment; using default"
        cp ${CONFIGURATIONS_DIRECTORY}/services.default.yml ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/services.yml
    else
        cp ${CONFIGURATIONS_DIRECTORY}/services.${E}.yml ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/services.yml
    fi
    print_status
    exit_on_error 'Could not create services.yml.'
else
    report "Using existing services.yml."
fi

# append local settings for Drupal
if [ ! -f ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.local.php ]; then
    if [ -f ${CONFIGURATIONS_DIRECTORY}/settings.local.php ]; then

        report "Attaching settings.local.php..."
        cp ${CONFIGURATIONS_DIRECTORY}/settings.local.php ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/
        print_status

        report "Making settings.local.php non-writable..."
        chmod a-w ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.local.php
        print_status

    else
        report "No settings.local.php found. Proceeding..."
    fi
else
    report "Using existing settings.local.php..."
fi

# append environment-based settings for Drupal
if [ ! -f ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.{E}.php ]; then
    if [ -f ${CONFIGURATIONS_DIRECTORY}/settings.${E}.php ]; then

        report "Attaching settings.${E}.php..."
        cp ${CONFIGURATIONS_DIRECTORY}/settings.${E}.php ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/
        print_status

        report "Making settings.${E}.php non-writable..."
        chmod a-w ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.${E}.php
        print_status

    else
        report "No settings.${E}.php found. Proceeding..."
    fi
else
    report "Using existing settings.${E}.php..."
fi




# if Drupal is not installed 
drush status bootstrap | grep -q Successful
if [ $? -ne 0 ] ; then



    set_stage 'INSTALL'



    # create default.settings.php
    if [ ! -f ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/default.settings.php ] ; then
        report "Creating default.settings.php..."
        cp ${CONFIGURATIONS_DIRECTORY}/settings.default.php ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/default.settings.php
        print_status
    fi


    # create settings.php
    if [ ! -f ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.php ] ; then
        report "Creating initial settings.php..."
        cp ${CONFIGURATIONS_DIRECTORY}/settings.default.php ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.php
        print_status
        exit_on_error 'Could not create settings.php.'
    fi


    # temporarily make settings.php writable 
    report "Making settings.php writable..."
    chmod u+w ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.php
    print_status
    exit_on_error 'Could not make settings.php writable.'


    # create files directory
    if [ ! -d ${DRUPAL_FILES_DIRECTORY_RELATIVE} ] ; then
        report "Creating ${DRUPAL_FILES_DIRECTORY_RELATIVE} directory..."
        mkdir ${DRUPAL_FILES_DIRECTORY_RELATIVE}
        print_status
        exit_on_error "Could not create ${DRUPAL_FILES_DIRECTORY_RELATIVE} directory."
    fi


    # assign files directory to web server
    report "Assigning ${DRUPAL_FILES_DIRECTORY_RELATIVE} to web server..."
    chown -R ${web_server_user}:${web_server_group} ${DRUPAL_FILES_DIRECTORY_RELATIVE}
    print_status
    exit_on_error "Could not assign ${DRUPAL_FILES_DIRECTORY_RELATIVE} to web server"


    # create database catalog
    report "Creating database ${MYSQL_DATABASE}..."
    mysqladmin \
        --host=$MYSQL_HOST \
        --user=root \
        --password=$MYSQL_ROOT_PASSWORD \
        create $MYSQL_DATABASE
    print_status
    # exit_on_error "Database '${MYSQL_DATABASE}' could not be created or it already exists."
    # database should already exist in its container according to docker library/mysql


    # create drupal user and grant database privileges to drupal user
    report "Assigning *all* privileges to Drupal's '${MYSQL_USER}' user for '${MYSQL_DATABASE}' database..."
    mysql \
        --user=root \
        --password=$MYSQL_ROOT_PASSWORD \
        --host=$MYSQL_HOST \
        --execute="$grant_query"
    print_status
    exit_on_error "Could not assign proper privileges to '${MYSQL_USER}' database user."


    # install Drupal
    report "Installing Drupal..."
    drush site-install minimal \
        --yes \
        --db-url="mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_HOST}/${MYSQL_DATABASE}" \
        --site-name=$DRUPAL_SITE_NAME \
        --account-name=$DRUPAL_ACCOUNT_NAME \
        --account-pass=$DRUPAL_ACCOUNT_PASS
    print_status
    # exit_on_error "Drupal installation failed."

    # revoke settings.php writability
    report "Making settings.php non-writable..."
    chmod a-w ${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/settings.php
    print_status


    # enable languages
    report "Enabling languages..."
    cat ${CONFIGURATIONS_DIRECTORY}/language-list | xargs -l1 drush language-add


else
    report "Drupal is already installed."
fi # end of installation


set_stage 'INIT'


# attach .htaccess in files
if [ -f .htaccess ]; then
    report "Renaming existing .htaccess"
    mv .htaccess htaccess.installed
    print_status
fi
report "Attaching .htaccess in html..."
cp ${CONFIGURATIONS_DIRECTORY}/${html_htaccess_basename} \
    .htaccess
print_status


# make files 777
report "Unlocking ${DRUPAL_FILES_DIRECTORY_RELATIVE}"
chmod 777 ${DRUPAL_FILES_DIRECTORY_RELATIVE}
print_status
exit_on_error "Could not unlock '${DRUPAL_FILES_DIRECTORY_RELATIVE}'."




set_stage 'MODS'

types=('modules' 'themes')
levels=('core' 'contrib' 'custom')
sources=("${BASE_CONFIGURATIONS_DIRECTORY}" "${CONFIGURATIONS_DIRECTORY}")
actions=('uninstall' 'enable')
# the following values depend on Drush reports; see drush pm-info
skip_statuses=('not installed' 'enabled')

for type in "${types[@]}" 
do
    for level in "${levels[@]}" 
    do
        for sourc_ in "${sources[@]}" 
        do

            # action/status indicator
            i=$((1)) #

            for action in "${actions[@]}" 
            do
                i=$(( ($i + 1) % 2 )) #

                report "${type}:${level}:${sourc_}:${action} ..."

                # put modules into array
                module_names=($(cat ${sourc_}/modules.yml | shyaml get-values ${type}.${level}.${action}))

                for module_name in "${module_names[@]}"
                do

                    module_status=$(drush pm-info --format=yaml ${module_name} |shyaml get-value ${module_name}.status)
                    module_version=$(drush pm-info --format=yaml ${module_name} |shyaml get-value ${module_name}.version)

                    # if module is already in wanted state, skip
                    if [ "$module_status" == "${skip_statuses[$i]}" ]; then
                        report "Module '${module_name}' is already ${module_status}. Skipping..."
                        print_status
                    else
                        report "Module '${module_name}'..."
                        # put module in proper state
                        drush pm-${action} -y ${module_name}
                        
                        # if module uses routing
                        if [ -f modules/${level}/${module_name}/${module_name}.routing.yml ]; then
                            # clear router cache
                            drush cache-clear router
                        fi

                        # clear drush cache
                        # drush cache-clear drush
                        report "${module_name}-${module_version} ${skip_statuses[$i]}."
                    fi
                    echo # print line after module
                done
            done
        done
    done
done


set_stage 'INIT'

# update database
report "Updating database..."
drush updb --entity-updates -y
print_status


# enable admin theme
report "Enabling admin theme"
base_theme=$(cat ${BASE_CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values themes.admin)
deriv_theme=$(cat ${CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values themes.admin)
theme_name=''
if [[ $deriv_theme ]]; then
    theme_name="${deriv_theme}"
elif [[ $base_theme ]]; then
    theme_name="${base_theme}"
else
    theme_name='bartik'
fi
drush config-set system.theme admin -y ${theme_name}


# enable default theme
report "Enabling default theme"
base_theme=$(cat ${BASE_CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values themes.default)
deriv_theme=$(cat ${CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values themes.default)
theme_name=''
if [[ $deriv_theme ]]; then
    theme_name="${deriv_theme}"
elif [[ $base_theme ]]; then
    theme_name="${base_theme}"
else
    theme_name='bartik'
fi
drush config-set system.theme default -y ${theme_name}



# attach .htaccess in files
report "Attaching .htaccess in files..."
cp ${CONFIGURATIONS_DIRECTORY}/${files_htaccess_basename} \
    ${DRUPAL_FILES_DIRECTORY_RELATIVE}/.htaccess
print_status


# lock public and private files
report "Assigning ${DRUPAL_FILES_DIRECTORY_RELATIVE} to web server..."
chown -R ${web_server_user}:${web_server_group} ${DRUPAL_FILES_DIRECTORY_RELATIVE}
print_status


report "Locking ${DRUPAL_FILES_DIRECTORY_RELATIVE}..."
chmod 755 ${DRUPAL_FILES_DIRECTORY_RELATIVE}
print_status


report "Assigning ${DRUPAL_PRIVATE_DIRECTORY_RELATIVE} to web server..."
chown -R ${web_server_user}:${web_server_group} ${DRUPAL_PRIVATE_DIRECTORY_RELATIVE}
print_status


report "Locking ${DRUPAL_PRIVATE_DIRECTORY_RELATIVE}"
chmod 700 ${DRUPAL_PRIVATE_DIRECTORY_RELATIVE}
print_status


report "Assigning project file tree to Apache..."
find . -not -user ${web_server_user} | xargs -n 50 chown -R ${web_server_user}:${web_server_group}
print_status

report "Starting apache..."
# start apache foreground
apache2-foreground