require 'open3'
require 'pp'
require 'yaml'

# current home
PROJECT_HOME = Dir.pwd

# define and load default project file
BASE_PROJECT_YAML_FILE = 'base/rake/conf/project.yml'
@project = YAML.load_file(BASE_PROJECT_YAML_FILE)

# define and override with project file
PROJECT_YAML_FILE = 'rake/conf/project.yml'
@project.merge!(YAML.load_file(PROJECT_YAML_FILE) || {})

# utility constants

## current environment
E = ENV['env'] || @project["current_environment"]

## project-environment prefix, used mainly by docker-compose
PREFIX = @project[@project["current_environment"]]["prefix"]

## short aliases
D = "sudo docker"
DEXEC = "#{D} exec"
DEXECI = "#{DEXEC} -i"
DEXECIT = "#{DEXECI} -t"
DC = "sudo docker-compose #{(PREFIX) ? "-p #{PREFIX}" : ""} -f #{@project['file_prefix']}.compose.yml -f #{@project['file_prefix']}.#{E}.compose.yml"
GIT = "git"

# attach docker compose config to project
@project["docker-compose"] = YAML.load(%x[#{DC} config])

# utility fields
@docker_services = @project["docker-compose"]["services"].keys


# load all libraries and tasks
%w{ 
    base/rake/lib 
    base/rake/conf
    base/rake/tasks
    rake/lib
    rake/conf
    rake/tasks
}.each do |path|
    Dir.glob("#{path}/**/*.rb").each { |r| load r}
    Dir.glob("#{path}/**/*.rake").each { |r| load r}
end
