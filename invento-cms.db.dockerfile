FROM mysql:5.7

WORKDIR /root

RUN apt-get update &&\

    apt-get install -y --no-install-recommends \
        # ca-certificates \
        # wget \
        curl \
        git \
        &&\

    # IMPORTANT: don't remove the asterisk; docker aufs bug
    apt-get clean \
    && \
    apt-get purge \
    && \
    rm -rf /var/lib/apt/lists/*


# install MySQL Tuner
RUN git clone https://github.com/major/MySQLTuner-perl.git && \
    chmod u+x MySQLTuner-perl/mysqltuner.pl &&\
    mv MySQLTuner-perl/mysqltuner.pl ./ && \
    rm -rf MySQLTuner-perl

# attach my.cnf
# ADD conf/db/*.cnf /etc/mysql/conf.d/