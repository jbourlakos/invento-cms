FROM drupal:8.4-apache

ARG SITES_DIRECTORY=/var/www
ARG WORKING_DIRECTORY=${SITES_DIRECTORY}/html
ARG CONFIGURATIONS_DIRECTORY=/root/conf
ARG BASE_CONFIGURATIONS_DIRECTORY=${CONFIGURATIONS_DIRECTORY}/base
ARG LOCAL_EXECUTABLES_DIRECTORY=/usr/local/bin
ARG DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE='sites/default'



WORKDIR $WORKING_DIRECTORY



# install required utilities

RUN apt-get update &&\

    apt-get install -y --no-install-recommends \
        wget \
        git \
        curl \
        mysql-client \
        zip \
        unzip \
        python \
        vim \
        cron \
        supervisor \
        &&\

    # IMPORTANT: don't remove the asterisk; docker aufs bug
    apt-get clean \
    && \
    apt-get purge \
    && \
    rm -rf /var/lib/apt/lists/*


# install shyaml, terminal yaml parser
RUN wget https://bootstrap.pypa.io/get-pip.py -O /tmp/get-pip.py && \
    python /tmp/get-pip.py --no-wheel && \
    pip install shyaml && \
    rm -rf /tmp/get-pip.py


# attach php.ini
COPY conf/web/php.ini ${CONFIGURATIONS_DIRECTORY}/

RUN mv ${CONFIGURATIONS_DIRECTORY}/php.ini /usr/local/etc/php/ && \
# install PHP extensions
docker-php-ext-install \
    bcmath


COPY base/conf/web/composer-requirements ${BASE_CONFIGURATIONS_DIRECTORY}/
COPY conf/web/composer-requirements ${CONFIGURATIONS_DIRECTORY}/

RUN \

# install composer
    EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig) && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');") && \
    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ] ; \
    then \
        >&2 echo 'ERROR: Invalid installer signature' ; \
        rm composer-setup.php ; \
        exit 1 ; \
    fi && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer --quiet && \
    rm composer-setup.php && \
    chmod a+x ${LOCAL_EXECUTABLES_DIRECTORY}/composer  \

&& \

# prepare composer for drupal
    ${LOCAL_EXECUTABLES_DIRECTORY}/composer config repositories.drupal composer https://packages.drupal.org/8 \

&& \

# install required components from composer for drupal
    { cat ${BASE_CONFIGURATIONS_DIRECTORY}/composer-requirements | xargs -r -n 1 composer require ; } \

&& \

# install required components from composer for drupal
    { cat ${CONFIGURATIONS_DIRECTORY}/composer-requirements | xargs -r -n 1 composer require ; }



COPY base/conf/web/* ${BASE_CONFIGURATIONS_DIRECTORY}/
COPY conf/web/* ${CONFIGURATIONS_DIRECTORY}/


# INSTALL DRUSH

# Download latest stable release using the code below or browse to github.com/drush-ops/drush/releases.
RUN \
wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.5.1/drush.phar && \
    chmod +x drush.phar && \
    mv drush.phar /usr/local/bin/drush


# make modules and themes directories
RUN    mkdir -p modules/contrib && \
    mkdir -p themes/contrib

# remove base contrib modules
RUN /bin/bash -c \
    'module_names=($(cat ${BASE_CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values modules.contrib.remove)); \
    for module_name in "${module_names[@]}"; do \
        module_dir="modules/contrib/${module_name}"; \
        if [ -d "${module_dir}" ]; then \
            echo "Removing directory ${module_dir}"; \
            rm -rf "${module_dir}"; \
        fi \
    done'
# download base contrib modules
RUN /bin/bash -c \
    'module_names=($(cat ${BASE_CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values modules.contrib.download)); \
    if [ ${#module_names[@]} -ne 0 ]; then \
        drush pm-download -y --package-handler=wget ${module_names[@]} ; \
    else \
        echo No base contrib modules downloaded.; \
    fi'
# download base contrib themes
RUN /bin/bash -c \
    'module_names=($(cat ${BASE_CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values themes.contrib.download)); \
    if [ ${#module_names[@]} -ne 0 ]; then \
        drush pm-download -y --package-handler=wget ${module_names[@]} ; \
    else \
        echo No base contrib themes downloaded.; \
    fi'


# remove derivative contrib modules
RUN /bin/bash -c  'module_names=($(cat ${CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values modules.contrib.remove)); \
    for module_name in "${module_names[@]}"; do \
        module_dir="modules/contrib/${module_name}"; \
        if [ -d "${module_dir}" ]; then \
            echo "Removing directory ${module_dir}"; \
            rm -rf "${module_dir}"; \
        fi \
    done'
# download derivative contrib modules
RUN /bin/bash -c \
    'module_names=($(cat ${CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values modules.contrib.download)); \
    if [ ${#module_names[@]} -ne 0 ]; then \
        drush pm-download -y --package-handler=wget ${module_names[@]} ; \
    else \
        echo No derivative contrib modules downloaded.; \
    fi'
# download derivative contrib themes
RUN /bin/bash -c \
    'module_names=($(cat ${CONFIGURATIONS_DIRECTORY}/modules.yml | shyaml get-values themes.contrib.download)); \
    if [ ${#module_names[@]} -ne 0 ]; then \
        drush pm-download -y --package-handler=wget ${module_names[@]} ; \
    else \
        echo No derivative contrib themes downloaded.; \
    fi'



# install required Drupal libraries
COPY base/conf/web/libraries.yml $BASE_CONFIGURATIONS_DIRECTORY/
COPY conf/web/libraries.yml $CONFIGURATIONS_DIRECTORY/
RUN mkdir -p libraries \
&& \
    cat ${BASE_CONFIGURATIONS_DIRECTORY}/libraries.yml | \
    shyaml key-values libraries |\
    grep ^- | \
    cut -d ' ' --complement -f 1 - | \
    xargs -r -L 1 -I{} sh -c {} && \
    cat ${CONFIGURATIONS_DIRECTORY}/libraries.yml | \
    shyaml key-values libraries |\
    grep ^- | \
    cut -d ' ' --complement -f 1 - | \
    xargs -r -L 1 -I{} sh -c {} \
    && \
true

# RUN drush libraries-download geocomplete
# RUN drush libraries-download jquery.inputmask
# RUN drush libraries-download jquery.intl-tel-input
# RUN drush libraries-download jquery.rateit
# RUN drush libraries-download select2
# RUN drush libraries-download signature_pad
# RUN drush libraries-download jquery.timepicker
# RUN drush libraries-download jquery.toggles
# RUN drush libraries-download jquery.word-and-character-counter



# add custom modules and themes
COPY base/code/modules modules/custom/
COPY code/modules modules/custom/
COPY base/code/themes themes/custom/
COPY code/themes themes/custom/




# set environment variables

ENV SITES_DIRECTORY=${SITES_DIRECTORY} \
    WORKING_DIRECTORY=${WORKING_DIRECTORY} \
    CONFIGURATIONS_DIRECTORY=${CONFIGURATIONS_DIRECTORY} \
    BASE_CONFIGURATIONS_DIRECTORY=${CONFIGURATIONS_DIRECTORY}/base \
    DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE='sites/default' \
    DRUPAL_FILES_DIRECTORY_RELATIVE="${DRUPAL_SITES_DEFAULT_DIRECTORY_RELATIVE}/files" \
    DRUPAL_PRIVATE_DIRECTORY_RELATIVE="${SITES_DIRECTORY}/private"

COPY base/code/bin/start-up.sh  ${LOCAL_EXECUTABLES_DIRECTORY}/
# prepare start-up script
RUN chmod a+x ${LOCAL_EXECUTABLES_DIRECTORY}/start-up.sh 


# setup cron

RUN echo "23 * * * * curl -s http://example.com/cron.php >/dev/null 2>&1" | crontab -

# setup supervisor
COPY ./conf/web/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# start supervisor
# CMD /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf

CMD ["start-up.sh"]
