# Invento CMS: content management system
Based on 
[Drupal 8](https://www.drupal.org/8), 
[PHP 7](http://php.net/manual/en/index.php), 
[Apache 2.2](http://httpd.apache.org/docs/2.2/), 
[MySQL 5.7](http://dev.mysql.com/doc/refman/5.7/en/) 
and 
[Debian Jessie](https://www.debian.org/intro/about).

Containment and deployment by [Docker Engine](https://www.docker.com/products/docker-engine).

Powered by 
[Git](https://git-scm.com/), 
[PHP Composer](https://getcomposer.org/), 
and 
[Drush](http://www.drush.org/en/master/).

## Development

### Requirements

 - Install [Git](https://git-scm.com/downloads)
     + Debian / Ubuntu / Elementary / Mint: `sudo apt-get install git`
 - Install [Docker Platform](https://www.docker.com/products/docker#/)
 - Install [Docker Compose](https://docs.docker.com/compose/install/)
 - Install [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
 - Install [Rake](rake.rubyforge.org)

### Start working on the project

 - `cd` to your favourite software development folder
 - `git clone https://bitbucket.org/invento-allstar/invento-news/`
     + Or, with default username: `git clone https://USERNAME@bitbucket.org/invento-allstar/invento-news/`
 - `cd invento-news`
 - *Optional:* Configure your Git name: `git config user.name "YOUR_FULL_NAME"`
 - *Optional:* Configure your Git email: `git config user.mail "YOUR_EMAIL_ADDRESS"`
 - Initialize Git submodules: `git submodule --init --recursive`
     + Git passwords (and usernames) may be required a few times
 - Start the Docker machines: `rake compose:up`
     + Docker image fetching and a docker image build process will initiate during the first boot.
     + It will fail if the the port 8080 is bound to another service (e.g. a natively running Apache).
 - Visit: `http://localhost` on your web browser

### Docker composition basic management

The project is heavily dependent on virtual machines created by Docker engine. 
A **composition** of a web server and a database server are used to deploy the 
project either locally or in remote production environments.

* Build container composition: `rake compose:build`
* Create container composition: `rake compose:up`
* Start existing container composition: `rake compose:start`
* Stop existing container composition: `rake compose:stop`
* Destroy container composition: `rake compose:down`

### Install Drupal on the container

Drupal installation initiates automatically on the first boot of the web container.

Login information in file: `env/web/development.env`

## Invento News and Docker Engine

### Docker image inheritance
The ***Web server (Web)*** component

 - debian:jessie
     + php:7.0-apache
         * drupal:8.2.4-apache
 
The ***Database server (DB)*** component

 - mysql:5.7

### Docker and Container auto-loading 

#### Docker Phases
- *Image description* (dockerfiles)
- Image construction from dockerfile (`docker build`)
    + Attaching files in the image with ADD/COPY directives.
- Container creation from image
- A created container boots (`docker start`)
    + Attaching files with volumes.
- A created container executes
    + Volumes are updated in almost real-time
- A created container shuts down (`docker stop`)
    + Volumes are detached.
- Container destruction (`docker rm`)
- Image destruction (`docker rmi`)

#### CMS Autoload Sequence
- System requirements (e.g. apt-get update / install)
- PHP-level dependencies (composer)
- Drupal dependencies (drush)
    - Drupal Modules
        - Drupal Content types
        - Drupal Taxonomies
        - Drupal User Roles and Privileges
        - Drupal Views
        - Drupal Blocks
        - Drupal Default Themes
